﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class Home
    {
        public int id_home { get; set; }
        public string name { get; set; }
        public string type_home { get; set; }
        public Address address { get; set; }
        public int perimeter { get; set; }
        public string distance { get; set; }

    }
}
