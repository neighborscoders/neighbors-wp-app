﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class Message
    {
        public int id_message { get; set; }
        public DateTime date { get; set; }
        public string message { get; set; }
        public User User { get; set; }
        public int seen { get; set; }
    }
}
