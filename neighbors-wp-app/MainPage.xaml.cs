﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using neighbors_wp_app.Resources;
using System.Windows.Media;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

using Microsoft.Phone.Notification;

namespace neighbors_wp_app
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructeur
        string username;
        string password;
        string uri;
        bool ready = false;
        ProgressBar bar = new ProgressBar();
        ProgressBar bar2 = new ProgressBar();

        public MainPage()
        {

            InitializeComponent();
            //BuildLocalizedApplicationBar();
            isConnected();
            /// Holds the push channel that is created or found.
            HttpNotificationChannel pushChannel;

            // The name of our push channel.
            string channelName = "ToastSampleChannel";
             pushChannel = HttpNotificationChannel.Find(channelName);

            // If the channel was not found, then create a new connection to the push service.
             if (pushChannel == null)
             {
                 this.ContentPanel.Children.Add(bar2);
                 pushChannel = new HttpNotificationChannel(channelName);

                 // Register for all the events before attempting to open the channel.
                 pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                 pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                 // Register for this notification only if you need to receive the notifications while your application is running.
                 pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                 pushChannel.Open();

                 // Bind this new channel for toast events.
                 pushChannel.BindToShellToast();
             }
             else
             {

                 getNewUri(pushChannel);
             }

        }

        public void setReady(){
            ready = true;
            this.ContentPanel.Children.Remove(bar2);
        }

        public void getNewUri(HttpNotificationChannel pushChannel){
                             // The channel was already open, so just register for all the events.
                 pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                 pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                 // Register for this notification only if you need to receive the notifications while your application is running.
                 pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                 // Display the URI for testing purposes. Normally, the URI would be passed back to your web service at this point.
                 System.Diagnostics.Debug.WriteLine(pushChannel.ChannelUri.ToString());
                 setReady();
                 this.uri = pushChannel.ChannelUri.ToString();
                 PhoneApplicationService.Current.State["uri"] = this.uri;

        }

        /// <summary>
        /// Sends the URI.
        /// </summary>
        /// <param name="uri">The URI.</param>
        void sendUri(string uri)
        {
            this.uri = uri;
            string url;
            url = Conf.SERVER_DOMAIN +"uri";
            WebRequest request = WebRequest.Create(url);
            HttpWebRequest httpReq = (HttpWebRequest)request;
            request.ContentType = "application/json; charset=utf-8";
            httpReq.Method = "POST";

            httpReq.BeginGetRequestStream(sendUriReq, httpReq);
        }

        /// <summary>
        /// Sends the URI request.
        /// </summary>
        /// <param name="result">The result.</param>
        public void sendUriReq(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                Stream postStream = preq.EndGetRequestStream(result);

                string jsonString = "{\"uri\":\""+this.uri+"\"}";
                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {
                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse2(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.ContentPanel.Children.Remove(bar);
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        }

        /// <summary>
        /// Handles the ChannelUriUpdated event of the PushChannel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NotificationChannelUriEventArgs"/> instance containing the event data.</param>
        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {

            Dispatcher.BeginInvoke(() =>
            {
                setReady();
                // Display the new URI for testing purposes.   Normally, the URI would be passed back to your web service at this point.
                this.uri = e.ChannelUri.ToString();
                PhoneApplicationService.Current.State["uri"] = this.uri;
            });
        }

        /// <summary>
        /// Handles the ErrorOccurred event of the PushChannel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NotificationChannelErrorEventArgs"/> instance containing the event data.</param>
        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            // Error handling logic for your particular application would be here.
            Dispatcher.BeginInvoke(() =>
                MessageBox.Text = String.Format("A push notification {0} error occurred.  {1} ({2}) {3}",
                    e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData)
                );
        }

        /// <summary>
        /// Handles the ShellToastNotificationReceived event of the PushChannel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NotificationEventArgs"/> instance containing the event data.</param>
        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            string relativeUri = string.Empty;

            message.AppendFormat("Notification received at {0}!:\n", DateTime.Now.ToShortTimeString());

            // Parse out the information that was part of the message.
            int paramCount = 0;
            foreach (string key in e.Collection.Keys)
            {
                if(paramCount <= 1)
                    message.AppendFormat("{0}\n", e.Collection[key]);
                paramCount++;
                if (string.Compare(
                    key,
                    "wp:Param",
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.CompareOptions.IgnoreCase) == 0)
                {
                    relativeUri = e.Collection[key];
                }
            }
      
            UIThread.Invoke(() =>
           {

            string[] parts = relativeUri.Split(new char[] { '?', '&' });
            string[] convIdParts = parts[1].Split(new char[] {'='});
            string id_conversation = convIdParts[1];
          //  var currentPage = ((App)Application.Current).RootFrame.Content as PhoneApplicationPage;
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            if (currentPage is ConversationPage)
            {
                ConversationPage currentConversationPage = (ConversationPage)currentPage;
                // Is good conversation ?
                if (PhoneApplicationService.Current.State.ContainsKey("currentConversationId") && PhoneApplicationService.Current.State["currentConversationId"] != null)
                {
                 //   Conversation currentConversation = (Conversation)PhoneApplicationService.Current.State["currentConversation"];
                    if (PhoneApplicationService.Current.State["currentConversationId"].ToString() == id_conversation)
                    {
                        // On est sur la bonne conversation
                        PhoneApplicationService.Current.State["notifedConversationId"] = id_conversation;
                        currentConversationPage.appendNewMessage(id_conversation);
                    }
                }
            }
            else
            {
             //   var queryString = string.Join(string.Empty, relativeUri.Split('?').Skip(1));
              //  System.Web.HttpUtility.ParseQueryString(queryString);

                //textBlockFrom.Text = "Navigated here from " + this.NavigationContext.QueryString["NavigatedFrom"];

                    if (System.Windows.MessageBoxResult.OK == System.Windows.MessageBox.Show(message.ToString()))
                    {
                        if (id_conversation != null)
                        {
                            // Aller vers la page de Conversation
                            PhoneApplicationService.Current.State["notifedConversationId"] = id_conversation;
                            if (NavigationService != null)
                            {
                                NavigationService.Navigate(new Uri("/ConversationPage.xaml", UriKind.Relative));
                            }
                        }
                    }
                    else
                    {
                        //do stuff if No
                    }
            }
        });




        }
        /// <summary>
        /// Ons the response2.
        /// </summary>
        /// <param name="response">The response.</param>
        private void onResponse2(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    UIThread.Invoke(() =>
                   {
                       this.MessageBox.Text = loginResponse.message;
                   });
                }
            }
            catch (WebException e)
            {
            }
        }

        // Exemple de code pour la conception d'une ApplicationBar localisée
        private void BuildLocalizedApplicationBar()
        {
            /* // Définit l'ApplicationBar de la page sur une nouvelle instance d'ApplicationBar.
             ApplicationBar = new ApplicationBar();
             ApplicationBar.BackgroundColor = Color.FromArgb(255,135,206,250);
             ApplicationBar.ForegroundColor = Colors.Black;

             // Crée un bouton et définit la valeur du texte sur la chaîne localisée issue d'AppResources.
             ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/Images/edit.png", UriKind.Relative));
             appBarButton.Text = AppResources.AppBarButtonText;
             ApplicationBar.Buttons.Add(appBarButton);

             appBarButton = new ApplicationBarIconButton(new Uri("/Assets/Images/favs.png", UriKind.Relative));
             appBarButton.Text = AppResources.AppBarButtonText;
             ApplicationBar.Buttons.Add(appBarButton);

             appBarButton = new ApplicationBarIconButton(new Uri("/Assets/Images/refresh.png", UriKind.Relative));
             appBarButton.Text = AppResources.AppBarButtonText;
             ApplicationBar.Buttons.Add(appBarButton);*/

        }

        /// <summary>
        /// Appelé lorsqu'une page devient la page active dans une frame.
        /// </summary>
        /// <param name="e">Objet qui contient les données d'événement.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            

            isConnected();

        }
        /// <summary>
        /// Hides the fields.
        /// </summary>
        public void hideFields()
        {
            this.tblLogin.Visibility = Visibility.Collapsed;
            this.tbLogin.Visibility = Visibility.Collapsed;
            this.tblPassword.Visibility = Visibility.Collapsed;
            this.tbPassword.Visibility = Visibility.Collapsed;
            this.bConnect.Visibility = Visibility.Collapsed;
            this.hbSubscribe.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// Determines whether this instance is connected.
        /// </summary>
        public void isConnected()
        {
            
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                hideFields();
                // Add loader bar
                if (!this.ContentPanel.Children.Contains(bar))
                {
                    bar.IsIndeterminate = true;
                    this.ContentPanel.Children.Add(bar);
                }
                string url;
                url = Conf.SERVER_DOMAIN + session + "/session";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                httpReq.BeginGetResponse(checkSession, request);
            }
        }

        /// <summary>
        /// Checks the session.
        /// </summary>
        /// <param name="result">The result.</param>
        public void checkSession(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    onResponse(response.GetResponseStream());
                }
                catch (WebException e)
                {
                    Console.WriteLine("Error session check");
                }
            }
        }

        /// <summary>
        /// Handles the Tap event of the hbSubscribe control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
        private void hbSubscribe_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneApplicationService.Current.State["uri"] = this.uri;
            NavigationService.Navigate(new Uri("/Subscribe.xaml", UriKind.Relative));
        }

        /// <summary>
        /// Handles the Click event of the bConnect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void bConnect_Click(object sender, RoutedEventArgs e)
        {
            if (this.uri == null)
            {
                
            }
            if (this.MessageBox.Text.Length > 0)
            {
                this.MessageBox.Text = "";
            }

            this.username = this.tbLogin.Text;
            this.password = this.tbPassword.Password;

            if (username.Length > 0 && password.Length > 0)
            {
                // Add loader bar
                bar.IsIndeterminate = true;
                this.ContentPanel.Children.Add(bar);

                string url;
                url = Conf.SERVER_DOMAIN + "login";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                request.ContentType = "application/json; charset=utf-8";
                httpReq.Method = "POST";

                httpReq.BeginGetRequestStream(postLogins, httpReq);
            }
            else
            {
                if (username.Length <= 0)
                {
                    this.tbLogin.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                if (password.Length <= 0)
                {
                    this.tbPassword.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                this.MessageBox.Text = "Invalid username or password";
            }
        }

        /// <summary>
        /// Posts the logins.
        /// </summary>
        /// <param name="result">The result.</param>
        private void postLogins(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                if (this.ready)
                {

                    Stream postStream = preq.EndGetRequestStream(result);

                    StringBuilder postParamBuilder = new StringBuilder();

                    string jsonString = "{ \"username\":\"" + this.username + "\", \"password\":\"" + this.password + "\", \"notif_uri\":\"" + this.uri + "\"}";

                    Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                    postStream.Write(byteArray, 0, byteArray.Length);
                    postStream.Close();

                    preq.BeginGetResponse((IAsyncResult final_result) =>
                        {
                            HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                            if (req != null)
                            {
                                try
                                {
                                    WebResponse response = req.EndGetResponse(final_result);
                                    onResponse(response.GetResponseStream());
                                }
                                catch (WebException e)
                                {
                                    UIThread.Invoke(() =>
                                    {
                                        this.ContentPanel.Children.Remove(bar);
                                        this.MessageBox.Text = "Network error, please try again later";
                                    });
                                }
                            }
                        }
                    , preq);
                }
            }
        }

        /// <summary>
        /// Ons the response.
        /// </summary>
        /// <param name="response">The response.</param>
        private void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                    //   List<object> data = (List<object>)loginResponse.data;    

                    User user = JsonConvert.DeserializeObject<User>(data[0].ToString());                    
                    List<Home> homes = JsonConvert.DeserializeObject<List<Home>>(data[1].ToString());
                    string session;
                    IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
                    if (String.IsNullOrEmpty(session))
                    {
                        session = JsonConvert.DeserializeObject<string>(data[2].ToString());
                        IsolatedStorageSettings.ApplicationSettings.Add("session", session);
                        IsolatedStorageSettings.ApplicationSettings.Save();
                    }
                  //  PhoneApplicationService.Current.State["session"] = session;
                    // passer à la page index avec les bonnes données
                    PhoneApplicationService.Current.State["user"] = user;
                    PhoneApplicationService.Current.State["userHomes"] = homes;
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                        if (loginResponse.message.Equals("Session is expired"))
                        {
                            IsolatedStorageSettings.ApplicationSettings.Remove("session");
                            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                        }
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.ContentPanel.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }

        /// <summary>
        /// Handles the TextChanged event of the tbLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void tbLogin_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox loginBlock = (TextBox)sender;
                loginBlock.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 43, 184, 96));
                this.MessageBox.Text = "";
            }
        }

        /// <summary>
        /// Handles the PasswordChanged event of the tbPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void tbPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (sender is PasswordBox)
            {
                PasswordBox passwordBlock = (PasswordBox)sender;
                passwordBlock.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 43, 184, 96));
                this.MessageBox.Text = "";
            }
        }


    }
}