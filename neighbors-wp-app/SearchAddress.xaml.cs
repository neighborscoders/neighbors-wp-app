﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Maps.Toolkit;
using Microsoft.Phone.Shell;
using neighbors_wp_app.BingMapGeoCodeService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using Windows.Devices.Geolocation;


namespace neighbors_wp_app
{
    public partial class SearchAddress : PhoneApplicationPage
    {
 
         // My current location
         private GeoCoordinate MyCoordinate = null;
 
         // Reverse geocode query
         private ReverseGeocodeQuery MyReverseGeocodeQuery = null;
 
         /// <summary>
         /// Accuracy of my current location in meters;
         /// </summary>
         private double _accuracy = 0.0;
        

        public SearchAddress()
        {
            InitializeComponent();
            PhoneApplicationService.Current.State["address"] = null;
            if (PhoneApplicationService.Current.State.ContainsKey("oldAddress") && PhoneApplicationService.Current.State["oldAddress"] != null)
            {
                Address oldAddress = (Address)PhoneApplicationService.Current.State["oldAddress"];
                PhoneApplicationService.Current.State["address"] = oldAddress.ToSlashString();
            }
        }

        //Select the address thanks to the GPS
        private void startLocationButton_Click(object sender, RoutedEventArgs e)
        {
            PhoneApplicationService.Current.State["address"] = null;
            GetCurrentCoordinate();            
        }
         
               /// <summary>
        /// Method to get current coordinate asynchronously so that the UI thread is not blocked. Updates MyCoordinate.
        /// Using Location API requires ID_CAP_LOCATION capability to be included in the Application manifest file.
        /// </summary>
        private async void GetCurrentCoordinate()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracy = PositionAccuracy.High;

            try
            {
                Geoposition currentPosition = await geolocator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
                _accuracy = currentPosition.Coordinate.Accuracy;

                MyCoordinate = new GeoCoordinate(currentPosition.Coordinate.Latitude, currentPosition.Coordinate.Longitude);

                if (MyReverseGeocodeQuery == null || !MyReverseGeocodeQuery.IsBusy)
                {
                    MyReverseGeocodeQuery = new ReverseGeocodeQuery();
                    MyReverseGeocodeQuery.GeoCoordinate = new GeoCoordinate(MyCoordinate.Latitude, MyCoordinate.Longitude);
                    MyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
                    MyReverseGeocodeQuery.QueryAsync();
                }
            }
            catch (Exception ex)
            {
                AskUserToTurnOnLocationServices();
            }
        }


        private void ReverseGeocodeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    MapAddress address = e.Result[0].Information.Address;

                    if (!PhoneApplicationService.Current.State.ContainsKey("address") || PhoneApplicationService.Current.State["address"] == null)
                    {
                        PhoneApplicationService.Current.State["address"] = this.AddressFormat(address);
                        ShowAddress(PhoneApplicationService.Current.State["address"].ToString());
                    }

                    Pushpin p = new Pushpin();
                    p.Content = FormatAddress(address);

                    Map myMap = this.map;
                    myMap.Center = MyCoordinate;
                    myMap.ZoomLevel = 15;

                    // Create a MapOverlay to contain the pushpin.
                    MapOverlay myLocationOverlay = new MapOverlay();
                    myLocationOverlay.Content = p;
                    myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
                    myLocationOverlay.GeoCoordinate = MyCoordinate;

                    // Create a MapLayer to contain the MapOverlay.
                    MapLayer myLocationLayer = new MapLayer();
                    myLocationLayer.Add(myLocationOverlay);

                    //add the layer
                    myMap.Layers.Add(myLocationLayer);
                }
            }
        }

        public String AddressFormat(MapAddress a)
        {
            return String.Format("{0}/{1}/{2}/{3}", a.HouseNumber, a.Street, a.PostalCode, a.City);
        }


        private static async void AskUserToTurnOnLocationServices()
        {
            var result = MessageBox.Show("This app needs to access your location data", "Would you like to turn on Location Services?", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
            }
        }

        private string FormatAddress(MapAddress address)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(address.Street).Append(",")
                .Append(address.HouseNumber).Append("\n")
                .Append(address.City).Append("\n")
                .Append(address.Country).Append("\n")
                .Append(address.Continent);

            return sb.ToString();
        }

        private string FormatCoordinates(Geoposition geoposition)
        {
            return String.Format("Latitude: {0}\nLongitude: {1}", geoposition.Coordinate.Latitude, geoposition.Coordinate.Longitude);
        }

        private void bLocateManually_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddressPage.xaml", UriKind.Relative));
        }

        private void tbResultAddress_Loaded(object sender, RoutedEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("address") && PhoneApplicationService.Current.State["address"] != null)
            {
                ShowAddress(PhoneApplicationService.Current.State["address"].ToString());
            }
        }

        public void ShowAddress(String a)
        {
            String[] address = a.Split('/');

            if (address.Length > 4)
            {
                this.tbResultAddress.Text = String.Format("Address : {0} {1} {2} {3} {4}",
                    address[0],
                    address[1],
                    address[2],
                    address[3],
                    address[4]);
            }
            else
            {
                this.tbResultAddress.Text = String.Format("Address : {0} {1} {2} {3}",
                    address[0],
                    address[1],
                    address[2],
                    address[3]);
            }
        }

        private void bValidAddress_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }

        }

        private async void map_Loaded(object sender, RoutedEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("address") && PhoneApplicationService.Current.State["address"] != null)
            {
                String[] address = PhoneApplicationService.Current.State["address"].ToString().Split('/');
                string uri = "http://dev.virtualearth.net/REST/v1/Locations/FR/{0}/{1}/{2}?includeNeighborhood=true&key={3}";
                string requestUri = string.Format(uri,
                    Uri.EscapeDataString(address[2]),
                    Uri.EscapeDataString(address[3]),
                    Uri.EscapeDataString(String.Format("{0} {1}", address[0], address[1])),
                    "AlB0XQy6qYHHAtRvzaKjOeZ2jnjHjbwPKeeByxztAKi8XXfOuydWz4mEtOpGovhq");

                HttpClient client = new HttpClient();
                var response = await client.GetAsync(requestUri);
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsAsync<neighbors_wp_app.LocationsFormat.BingLocationResult>();

                try
                {
                    MyCoordinate = new GeoCoordinate(result.resourceSets[0].resources[0].geocodePoints[0].coordinates[0], result.resourceSets[0].resources[0].geocodePoints[0].coordinates[1]);

                    if (MyReverseGeocodeQuery == null || !MyReverseGeocodeQuery.IsBusy)
                    {
                        MyReverseGeocodeQuery = new ReverseGeocodeQuery();
                        MyReverseGeocodeQuery.GeoCoordinate = new GeoCoordinate(MyCoordinate.Latitude, MyCoordinate.Longitude);
                        MyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
                        MyReverseGeocodeQuery.QueryAsync();
                    }
                }
                catch (COMException ex)
                {
                    AskUserToTurnOnLocationServices();
                }
                catch (IndexOutOfRangeException ex)
                {
                    LocationNotFound();
                }
            }
        }

        private static void LocationNotFound()
        {
            var result = MessageBox.Show("The address was not found on the map, please re-try or use the \"Locate Me\" button.");
        }

        private void bValidAddress_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            String[] address = PhoneApplicationService.Current.State["address"].ToString().Split('/');

            Address newAddress = new Address();
            newAddress.number = int.Parse(address[0]);
            newAddress.street = address[1];
            newAddress.zipcode = int.Parse(address[2]);
            newAddress.city = address[3];
            if(MyCoordinate != null){
                newAddress.lat = MyCoordinate.Latitude;
                newAddress.lon = MyCoordinate.Longitude;
            }


            Home newHome;
            if (PhoneApplicationService.Current.State.ContainsKey("home") && PhoneApplicationService.Current.State["home"] != null)
            {
                newHome = (Home)PhoneApplicationService.Current.State["home"];
                newHome.address = newAddress;
            }
            else
            {
                newHome = new Home();
                newHome.address = newAddress;
            }

            PhoneApplicationService.Current.State["newHome"] = newHome;


            NavigationService.Navigate(new Uri("/HomeSettings.xaml", UriKind.Relative));
        }
    }
}