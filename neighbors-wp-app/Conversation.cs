﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class Conversation
    {
        public int id_conversation { get; set; }
        public List<User> users { get; set; }
        public string last_message_datetime { get; set; }
        public string last_message_seen { get; set; }
    }
}
