﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;


namespace neighbors_wp_app
{
    public partial class Index : PhoneApplicationPage
    {
        public Index()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("user") && PhoneApplicationService.Current.State["user"] != null)
            {
                updateUserInfo();
            }
            if (PhoneApplicationService.Current.State.ContainsKey("userHomes") && PhoneApplicationService.Current.State["userHomes"] != null)
            {
                displayHomeList();
            }
            UIThread.Invoke(() =>
            {
                if (this.InboxHome.Children.Count() >= 0)
                {
                    this.InboxHome.Children.Clear();
                    this.InboxHome.Children.Add(new Inbox(this));
                }
            });

        }

        private void bNewLocation_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SearchAddress.xaml", UriKind.Relative));
        }

        private void displayHome_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (sender is HomeCard)
            {
                HomeCard homeCard = (HomeCard)sender;
                PhoneApplicationService.Current.State["home"] = homeCard.Home;
            }
            NavigationService.Navigate(new Uri("/HomePage.xaml", UriKind.Relative));
        }

        public void updateUserInfo()
        {
            User currentUser = (User)PhoneApplicationService.Current.State["user"];            
            
            if (currentUser.photo_path != null)
            {                
                this.UserPicture.Source = new BitmapImage(new Uri(currentUser.photo_path));
            }
        }

        private void displayHomeList()
        {
            this.LocationList.Children.Clear();
            List<Home> homes = (List<Home>)PhoneApplicationService.Current.State["userHomes"];
            foreach (Home home in homes)
            {
                HomeCard homeCard = new HomeCard(home);
                homeCard.HomeName.Text = home.name;
                homeCard.HomeAddress.Text = home.address.ToString();
                homeCard.Tap += displayHome_Tap;
                this.LocationList.Children.Add(homeCard);
            }
        }        

        private void UserPicture_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {             
            NavigationService.Navigate(new Uri("/UserSettings.xaml", UriKind.Relative));
        }
    }
}