﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class Conversation2
    {
        public int id_conversation { get; set; }
        public User user1 { get; set; }
        public User user2 { get; set; }
        public Home home1 { get; set; }
        public Home home2 { get; set; }
    }
}
