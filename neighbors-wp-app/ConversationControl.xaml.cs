﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text;
using System.Windows.Media.Imaging;

namespace neighbors_wp_app
{
    public partial class ConversationControl : UserControl
    {
        public Conversation conversation {get; set;}
        public ConversationControl()
        {
            InitializeComponent();
        }

        public ConversationControl(Conversation conversation) : this()
        {
            this.conversation = conversation;
            StringBuilder sb = new StringBuilder();
            int userCount = 0;
            foreach (User user in conversation.users)
            {
                userCount++;
                sb.AppendFormat("{0} ", user.Firstname);
                if(userCount == 1){
                    this.ProfilePicture.Source = new BitmapImage(new Uri(user.photo_path));
                }
            }
            this.ConversationName.Text = sb.ToString();
            if(conversation.last_message_datetime != null){
                this.ConversationSubtitle.Text = conversation.last_message_datetime.ToString();
            }
            else
            {
                this.ConversationSubtitle.Text = "no messages";
            }

            if (this.conversation.last_message_seen.Equals("0"))
            {
                this.ConversationName.FontWeight = FontWeights.Bold;
                this.ConversationSubtitle.FontWeight = FontWeights.Bold;
            }
            else
            {
                this.ConversationName.FontWeight = FontWeights.Normal;
                this.ConversationSubtitle.FontWeight = FontWeights.Normal;
            }
        }
    }
}
