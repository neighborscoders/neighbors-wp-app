﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;

namespace neighbors_wp_app
{
    public partial class AddressPage : PhoneApplicationPage
    {

        public AddressPage()
        {
            InitializeComponent();
            SetFields();
        }
        public void SetFields()
        {
            if (PhoneApplicationService.Current.State.ContainsKey("oldAddress") && PhoneApplicationService.Current.State["oldAddress"] != null)
            {
                Address address = (Address)PhoneApplicationService.Current.State["oldAddress"];
                this.tbNum.Text = address.number.ToString();
                this.tbStreet.Text = address.street;

                if (!String.IsNullOrEmpty(address.street2))
                {
                    this.tbStreet2.Text = address.street2;
                }

                this.tbZip.Text = address.zipcode.ToString();
                this.tbCity.Text = address.city;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bAddress_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.tbCity.Text) && !String.IsNullOrEmpty(this.tbZip.Text) && !String.IsNullOrEmpty(this.tbNum.Text) && !String.IsNullOrEmpty(this.tbStreet.Text))
            {
                String fullAddress = "";

                if (!String.IsNullOrEmpty(this.tbStreet2.Text))
                {
                    fullAddress = String.Format("{0}/{1}/{2}/{3}/{4}", this.tbNum.Text, this.tbStreet.Text, this.tbStreet2.Text, this.tbZip.Text, this.tbCity.Text);
                }
                else
                {
                    fullAddress = String.Format("{0}/{1}/{2}/{3}", this.tbNum.Text, this.tbStreet.Text, this.tbZip.Text, this.tbCity.Text);
                }
                PhoneApplicationService.Current.State["address"] = fullAddress;
                NavigationService.GoBack();
            }
            else
            {
                if (this.tbNum.Text.Length <= 0)
                {
                    this.tbNum.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    this.tbNum.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 21, 158, 150));
                }
                if (this.tbStreet.Text.Length <= 0)
                {
                    this.tbStreet.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    this.tbNum.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 21, 158, 150));
                }
                if (this.tbZip.Text.Length <= 0)
                {
                    this.tbZip.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    this.tbNum.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 21, 158, 150));
                }
                if (this.tbCity.Text.Length <= 0)
                {
                    this.tbCity.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    this.tbNum.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 21, 158, 150));
                }
                this.MessageBox.Text = "Invalid or empty fields";
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox field = (TextBox)sender;
                if (field.Text.Length <= 0)
                {
                    field.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                else
                {
                    field.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 21, 158, 150));
                }
            }
        }

        private void ContentPanel_Loaded(object sender, RoutedEventArgs e)
        {
        }
        
    }
}