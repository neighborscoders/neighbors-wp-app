﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace neighbors_wp_app
{
    public partial class OccupantPage : PhoneApplicationPage
    {
        public OccupantPage()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (PhoneApplicationService.Current.State.ContainsKey("selectedNeighborHome") && PhoneApplicationService.Current.State["selectedNeighborHome"] != null)
            {
                updateHomeInfo();
            }
        }

        public void setHomeTypeImage(Home home)
        {
            switch (home.type_home)
            {
                case "Flat":
                    this.HomeTypeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-building.jpg", UriKind.Relative));
                    break;
                case "House":
                    this.HomeTypeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-house.jpg", UriKind.Relative));
                    break;
            }
        }

        public void loadOccupants(Home home)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("users") && PhoneApplicationService.Current.State["users"] != null)
            {
                List<User> users = (List<User>)PhoneApplicationService.Current.State["users"];
                if (users != null)
                {
                    int usersCount = 0;
                    UIThread.Invoke(() =>
                    {
                        if (this.OccupantsList.Children.Count >= 0)
                        {
                            this.OccupantsList.Children.Clear();
                        }
                    });

                    foreach (User currentUser in users)
                    {
                        usersCount++;
                        UIThread.Invoke(() =>
                        {
                            // Remove loader bar
                            UserCard userCardToAdd = new UserCard(currentUser);
                            userCardToAdd.Tap += userCard_Tap;
                            this.OccupantsList.Children.Add(userCardToAdd);
                        });
                    }
                    if (usersCount <= 0)
                    {
                        UIThread.Invoke(() =>
                        {
                            TextBlock text = new TextBlock();
                            text.Text = "There is no occupants found";
                            this.OccupantsList.Children.Add(text);
                        });
                    }
                }
                else
                {
                    UIThread.Invoke(() =>
                    {
                        TextBlock text = new TextBlock();
                        text.Text = "There is no occupants found";
                        this.OccupantsList.Children.Add(text);
                    });
                }
            }
        }

        private void userCard_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            UserCard userCard = (UserCard)sender;
            User tappedUser = userCard.User;
            PhoneApplicationService.Current.State["tappedUser"] = tappedUser;
           // PhoneApplicationService.Current.State["selec"]
            NavigationService.Navigate(new Uri("/Services.xaml", UriKind.Relative));        
        }
        public void updateHomeInfo()
        {
            Home currentHome = (Home)PhoneApplicationService.Current.State["selectedNeighborHome"];
            setHomeTypeImage(currentHome);
            this.HomeName.Text = currentHome.name;
            loadOccupants(currentHome);

        }
    }
}