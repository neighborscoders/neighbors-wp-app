﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace neighbors_wp_app
{
    public partial class HomePage : PhoneApplicationPage
    {

        ProgressBar bar = new ProgressBar();

        public HomePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("home") && PhoneApplicationService.Current.State["home"] != null)
            {
                updateHomeInfo();
            }
        }

        public void displayNeighbor_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

        }

        public void homeSettings_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/HomeSettings.xaml", UriKind.Relative));
        }
        public void neighborCard_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            HomeCard neighbor = (HomeCard)sender;
            PhoneApplicationService.Current.State["selectedNeighborHome"] = neighbor.Home;
          //  NavigationService.Navigate(new Uri("/OccupantPage.xaml", UriKind.Relative));

            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/home/" + neighbor.Home.id_home + "/occupants";
                WebRequest request;
                request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                httpReq.BeginGetResponse(getNeighborUser, request);
            }
        }

        public void getNeighborUser(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    onNeighborUserResponse(response.GetResponseStream());
                }
                catch (WebException e)
                {
                    Console.WriteLine("Neighbors list not found");
                }
            }
        }

        public void onNeighborUserResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<User> users = JsonConvert.DeserializeObject<List<User>>(loginResponse.data.ToString());

                    PhoneApplicationService.Current.State["users"] = users;
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        NavigationService.Navigate(new Uri("/OccupantPage.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.ContentPanel.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }

        public void updateHomeInfo()
        {
            Home currentHome = (Home)PhoneApplicationService.Current.State["home"];
            setHomeTypeImage(currentHome);
            this.HomeName.Text = currentHome.name;
            loadNeighbors(currentHome);

        }
        public void setHomeTypeImage(Home home)
        {
            switch (home.type_home)
            {
                case "Flat":
                    this.HomeTypeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-building.jpg", UriKind.Relative));
                    break;
                case "House":
                    this.HomeTypeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-house.jpg", UriKind.Relative));
                    break;
            }
        }

        public void loadNeighbors(Home home)
        {
            // Add loader bar
            bar.IsIndeterminate = true;
            this.NeiborsList.Children.Add(bar);

            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/home/" + home.id_home + "/neighbors";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                //request.ContentType = "application/json; charset=utf-8";
                //request.Method = "GET";
                try
                {
                    httpReq.BeginGetResponse(getNeighborsList, httpReq);
                }
                catch (WebException e)
                {

                }
            }

        }

        public void getNeighborsList(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());
                    String resut = streamReader.ReadToEnd();
                    List<Home> neighbors = new List<Home>();
                    neighbors = JsonConvert.DeserializeObject<List<Home>>(resut);
                    displayNeighbors(neighbors);
                }
                catch (WebException e)
                {
                    Console.WriteLine("Neighbors list not found");
                }
            }
        }

        public void displayNeighbors(List<Home> neighbors)
        {
            if (neighbors != null)
            {
                int neighborsCount = 0;
                UIThread.Invoke(() =>
                {
                    if (this.NeiborsList.Children.Count >= 0)
                    {
                        this.NeiborsList.Children.Clear();
                    }
                });

                foreach (Home currentHome in neighbors)
                {
                    neighborsCount++;
                    UIThread.Invoke(() =>
                    {
                        // Remove loader bar
                        HomeCard homeCardToAdd = new HomeCard(currentHome);
                        homeCardToAdd.Tap += neighborCard_Tap;
                        this.NeiborsList.Children.Add(homeCardToAdd);
                    });
                }
                if (neighborsCount <= 0)
                {
                    UIThread.Invoke(() =>
                    {
                        TextBlock text = new TextBlock();
                        text.Text = "There is no neighbors found";
                        this.NeiborsList.Children.Add(text);
                    });
                }
            }
            else
            {
                UIThread.Invoke(() =>
                {
                    TextBlock text = new TextBlock();
                    text.Text = "There is no neighbors found";
                    this.NeiborsList.Children.Add(text);
                });
            }
        }
    }
}