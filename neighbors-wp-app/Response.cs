﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
   public class Response
    {
        public string state { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}