﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace neighbors_wp_app
{
    public partial class UserSettings : PhoneApplicationPage
    {
        private User userToModify;

        public UserSettings()
        {
            InitializeComponent();
            SetFields();
        }

        public void SetFields()
        {
            if (PhoneApplicationService.Current.State.ContainsKey("user") && PhoneApplicationService.Current.State["user"] != null)
            {
                User user = (User)PhoneApplicationService.Current.State["user"];                
                this.tbFirstName.Text = user.Firstname;
                this.tbLastName.Text = user.Lastname;
                this.tbBirthdate.Value = user.BirthDate;
                this.imgProfil.Source = new BitmapImage(new Uri(user.photo_path));
            }
        }

        private void bChangePicture_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddPhoto.xaml", UriKind.Relative));
        }

        private void imgProfil_Loaded(object sender, RoutedEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("photo") && PhoneApplicationService.Current.State["photo"] != null)
            {
                this.imgProfil.Source = (BitmapImage)PhoneApplicationService.Current.State["photo"];
            }
        }

        private void bSaveChanges_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
            if (this.tbFirstName.Text.Length >= 0 && this.tbLastName.Text.Length >= 0)
            {                
                User currentUser = (User)PhoneApplicationService.Current.State["user"];
                currentUser.Firstname = this.tbFirstName.Text;
                currentUser.Lastname = this.tbLastName.Text;
                currentUser.BirthDate = this.tbBirthdate.Value.Value;                
                
                if (!String.IsNullOrEmpty(this.pbOldPassword.Password) && !String.IsNullOrEmpty(this.pbNewPassword.Password))
                {
                    if (this.pbOldPassword.Password.Equals(currentUser.Password))
                    {
                        currentUser.Password = this.pbNewPassword.Password;
                        this.MessageBox.Text = "";
                    }
                    else
                    {
                        this.pbOldPassword.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                        this.MessageBox.Text = "Invalid Password";
                    }
                }

                UIThread.Invoke(() =>
                {
                    BitmapImage bitmapImage = (BitmapImage)imgProfil.Source;
                    if (bitmapImage != null)
                    {
                        currentUser.Image = this.ImageBase64(bitmapImage);
                    }
                });
                //     newUser.Image = this.imgProfil

                // Add loader bar
                /*bar.IsIndeterminate = true;
                this.ContentPanel.Children.Add(bar);*/

                userToModify = currentUser;

                string session;
                IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
                if (!String.IsNullOrEmpty(session))
                {
                    string url2;
                    url2 = Conf.SERVER_DOMAIN + session + "/session";
                    WebRequest request2 = WebRequest.Create(url2);
                    HttpWebRequest httpReq2 = (HttpWebRequest)request2;
                    httpReq2.BeginGetResponse(checkSession, request2);
                }               

                string url;
                url = Conf.SERVER_DOMAIN + session + "/edit";                
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                request.ContentType = "application/json; charset=utf-8";
                httpReq.Method = "POST";

                httpReq.BeginGetRequestStream(registerUser, httpReq);
            }
            else
            {
                if (this.tbFirstName.Text.Length <= 0)
                {
                    this.tbFirstName.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                if (this.tbLastName.Text.Length <= 0)
                {
                    this.tbLastName.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }

                this.MessageBox.Text = "These fields are required";
            }
        }

        public void checkSession(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                }
                catch (WebException e)
                {
                    Console.WriteLine("Error session check");
                }
            }
        }

        public void registerUser(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                Stream postStream = preq.EndGetRequestStream(result);

                StringBuilder postParamBuilder = new StringBuilder();

                string jsonString = JsonConvert.SerializeObject(userToModify);                
                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {
                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        }

        public void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                    //   List<object> data = (List<object>)loginResponse.data; 

                    User user = JsonConvert.DeserializeObject<User>(data[0].ToString());
                    List<Home> homes = JsonConvert.DeserializeObject<List<Home>>(data[1].ToString());
                    string session;
                    IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
                    if (String.IsNullOrEmpty(session))
                    {
                        session = JsonConvert.DeserializeObject<string>(data[2].ToString());
                        IsolatedStorageSettings.ApplicationSettings.Add("session", session);
                        IsolatedStorageSettings.ApplicationSettings.Save();
                    }
                    PhoneApplicationService.Current.State["user"] = user;
                    PhoneApplicationService.Current.State["userHomes"] = homes;
                    UIThread.Invoke(() =>
                    {
                        NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.MessageBox.Text = "Server response error";
                });
            }
        }


        public BitmapImage base64Image(string base64string)
        {
            byte[] fileBytes = Convert.FromBase64String(base64string);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }

        public string ImageBase64(BitmapImage bitmapImage)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                WriteableBitmap btmMap = new WriteableBitmap(bitmapImage);

                // write an image into the stream
                Extensions.SaveJpeg(btmMap, ms, btmMap.PixelWidth, btmMap.PixelHeight, 0, 100);

                ImageSourceConverter imageConverter = new ImageSourceConverter();

                string imageString = Convert.ToBase64String(ms.ToArray());
                return imageString;
            }
        }

        private void Logout_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/logout";
                WebRequest request;
                request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                httpReq.BeginGetResponse(logoutUser, request);
            }
        }

        public void logoutUser(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    UIThread.Invoke(() =>
                    {
                        IsolatedStorageSettings.ApplicationSettings.Remove("session");
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    });
                }
                catch (WebException e)
                {
                    Console.WriteLine("Web error");
                }
            }
        }
    }
}