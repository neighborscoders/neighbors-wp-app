﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace neighbors_wp_app
{
    public class UserCard : UserCardControl
    {
        string PicturePath;
        public User User;
        public UserCard()
        {
            addComponents();
        }
        public UserCard(User user) : this()
        {
            this.UserImage.Source = new BitmapImage(new Uri(user.photo_path));
            this.UserName.Text = user.Firstname +" "+user.Lastname;
            this.UserPseudo.Text = user.Username;
            this.User = user;
            
        }
        private void addComponents()
        {
            
            Grid.SetColumn(this.UserName, 1);
            Grid.SetRow(this.UserName, 0);

            Grid.SetColumn(this.UserPseudo, 1);
            Grid.SetRow(this.UserPseudo, 0);
          
        }

        

    }
}

