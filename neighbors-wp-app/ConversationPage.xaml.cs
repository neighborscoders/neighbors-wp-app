﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace neighbors_wp_app
{
    public partial class ConversationPage : PhoneApplicationPage
    {

        ProgressBar bar = new ProgressBar();
        string messageToSend;
        public ConversationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            InitPage();
        }

        public void InitPage()
        {
            // Lorsqu'on clique sur une conversation
            if (PhoneApplicationService.Current.State.ContainsKey("currentConversation") && PhoneApplicationService.Current.State["currentConversation"] != null)
            {
                OnClickedConversation();
            }

            // Lorsqu'une notification mène à cette page
            if (PhoneApplicationService.Current.State.ContainsKey("notifedConversationId") && PhoneApplicationService.Current.State["notifedConversationId"] != null)
            {
                OnNotification();
                PhoneApplicationService.Current.State["notifedConversationId"] = null;
            } 

            // Lorsqu'on arrive d'un arrive d'hors la conversation
            if (this.NavigationContext.QueryString.ContainsKey("id_conversation") && this.NavigationContext.QueryString["id_conversation"] != null)
            {
                OnOutsideFromApp();
                this.NavigationContext.QueryString["id_conversation"] = null;
            }
        }

        public void appendNewMessage(string conversationId){
            PhoneApplicationService.Current.State["notifedConversationId"] = conversationId;
            OnNotification();
        }

        public void OnClickedConversation()
        {
            Conversation currentConversation = (Conversation)PhoneApplicationService.Current.State["currentConversation"];
            PhoneApplicationService.Current.State["currentConversationId"] = currentConversation.id_conversation.ToString();
            int userCount = 0;
            foreach (User user in currentConversation.users)
            {
                userCount++;
                if (userCount == 1)
                {
                    this.ConversationName.Text = "Chat with " + user.Firstname;
                }
            }
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/conversation/" + currentConversation.id_conversation;
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                //request.ContentType = "application/json; charset=utf-8";
                //request.Method = "GET";
                try
                {
                    httpReq.BeginGetResponse(getMessages, httpReq);
                }
                catch (WebException webex)
                {
                    Console.WriteLine(webex.ToString());
                }
            }
        }

        public void OnNotification()
        {
            string conversationId = (string)PhoneApplicationService.Current.State["notifedConversationId"];
            PhoneApplicationService.Current.State["currentConversationId"] = conversationId;
            PhoneApplicationService.Current.State["conversationId"] = conversationId;
            displayNotifiedConversation(conversationId);
        }

        public void OnOutsideFromApp()
        {
            string conversationId = (string)this.NavigationContext.QueryString["id_conversation"];
            PhoneApplicationService.Current.State["conversationId"] = conversationId;
            PhoneApplicationService.Current.State["currentConversationId"] = conversationId;
            displayNotifiedConversation(conversationId);
        }


        public void displayNotifiedConversation(string conversationId){
                this.ConversationName.Text = "Have a chat!";
                string session;
                IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
                if (!String.IsNullOrEmpty(session))
                {
                    string url;
                    url = Conf.SERVER_DOMAIN + session + "/conversation/" + conversationId;
                    WebRequest request = WebRequest.Create(url);
                    HttpWebRequest httpReq = (HttpWebRequest)request;
                    try
                    {
                        httpReq.BeginGetResponse(getMessages, httpReq);
                    }
                    catch (WebException webex)
                    {
                        Console.WriteLine(webex.ToString());
                    }
                }
        }

        public void getMessages(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    onMessagesResponse(response.GetResponseStream());
                }
                catch (WebException e)
                {
                    Console.WriteLine("Messages list not found");
                }
            }
        }

        public void onMessagesResponse(Stream response){
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<Message> messages = JsonConvert.DeserializeObject<List<Message>>(loginResponse.data.ToString());

                    displayMessages(messages);
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.MessageList.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.MessageList.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }

        public void displayMessages(List<Message> messages)
        {
            if (messages.Count >= 0)
            {
                User user;
                User otherUser;
                if(PhoneApplicationService.Current.State.ContainsKey("user") && PhoneApplicationService.Current.State["user"] != null){
                    user = (User)PhoneApplicationService.Current.State["user"];

                    UIThread.Invoke(() =>
                    {
                        this.MessageList.Children.Clear();
                        foreach (Message message in messages)
                        {
                            addMessage(user, message);
                        }

                        this.ChatScrollViewer.Measure(this.ChatScrollViewer.RenderSize);
                        this.ChatScrollViewer.ScrollToVerticalOffset(this.ChatScrollViewer.ScrollableHeight);
                    });
                }
                else
                {
                    Message lastMessage = messages.Last();
                    otherUser = lastMessage.User;

                    UIThread.Invoke(() =>
                    {
                        this.MessageList.Children.Clear();
                        foreach (Message message in messages)
                        {
                            addMessageFromNotif(otherUser, message);
                        }

                        this.ChatScrollViewer.Measure(this.ChatScrollViewer.RenderSize);
                        this.ChatScrollViewer.ScrollToVerticalOffset(this.ChatScrollViewer.ScrollableHeight);
                    });
                }
      
            }
            else
            {
                this.MessageBox.Text = "There is no messages";
            }
        }

        private void SendButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.MessageInput.Text.Count() >= 0)
            {
                // Send message;
                this.messageToSend = this.MessageInput.Text;
                string conversationId;
                if (PhoneApplicationService.Current.State.ContainsKey("currentConversation") && PhoneApplicationService.Current.State["currentConversation"] != null)
                {
                    Conversation currentConversation = (Conversation)PhoneApplicationService.Current.State["currentConversation"];
                    conversationId = currentConversation.id_conversation.ToString();
                    setOneMessage(conversationId);
                } else if (PhoneApplicationService.Current.State.ContainsKey("conversationId") && PhoneApplicationService.Current.State["conversationId"] != null)
                {
                    conversationId = (string)PhoneApplicationService.Current.State["conversationId"];
                    setOneMessage(conversationId);
                }
             }
        }

        public void setOneMessage(string conversationId)
        {
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/conversation/" + conversationId + "/send";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                request.ContentType = "application/json; charset=utf-8";
                httpReq.Method = "POST";

                httpReq.BeginGetRequestStream(sendMessage, httpReq);
            }
        }

        public void addMessage(User user, Message message){
            if (user != null)
            {
                if (message.User.id_user == user.id_user)
                {
                    this.MessageList.Children.Add(new MessageControlRight(message));
                }
                else
                {
                    this.MessageList.Children.Add(new MessageControlLeft(message));
                }
            }
            else
            {
                this.MessageList.Children.Add(new MessageControlLeft(message));
            }
        }

        public void addMessageFromNotif(User otherUser, Message message)
        {
            if (otherUser != null)
            {
                if (message.User.id_user == otherUser.id_user)
                {
                    this.MessageList.Children.Add(new MessageControlLeft(message));
                }
                else
                {
                    this.MessageList.Children.Add(new MessageControlRight(message));                   
                }
            }
            else
            {
                this.MessageList.Children.Add(new MessageControlLeft(message));
            }
        }


        public void sendMessage(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                Stream postStream = preq.EndGetRequestStream(result);

                string jsonString = "{\"message\":\"" + messageToSend + "\"}";

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {
                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.MessageList.Children.Remove(bar);
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        }

        private void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    Message RecievedMessage = JsonConvert.DeserializeObject<Message>(loginResponse.data.ToString());
                    User user;

                    user = RecievedMessage.User;

                    UIThread.Invoke(() =>
                    {
                        this.MessageList.Children.Remove(bar);
                        this.MessageInput.Text = "";
                        addMessage(user, RecievedMessage); ;

                        this.ChatScrollViewer.Measure(this.ChatScrollViewer.RenderSize);
                        this.ChatScrollViewer.ScrollToVerticalOffset(this.ChatScrollViewer.ScrollableHeight);
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.MessageList.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.MessageList.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }
    }
}