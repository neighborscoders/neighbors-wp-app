﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using Newtonsoft.Json;

namespace neighbors_wp_app
{
    public partial class Services : PhoneApplicationPage
    {
        public Services()
        {
            InitializeComponent();
        }

        private void BorrowHubTile_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("selectedNeighborHome") && PhoneApplicationService.Current.State["selectedNeighborHome"] != null
            && PhoneApplicationService.Current.State.ContainsKey("home") && PhoneApplicationService.Current.State["home"] != null
            && PhoneApplicationService.Current.State.ContainsKey("tappedUser") && PhoneApplicationService.Current.State["tappedUser"] != null)
            {
                NavigationService.Navigate(new Uri("/Loan.xaml", UriKind.Relative));
            }
        }

        private void Talk_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                if (PhoneApplicationService.Current.State.ContainsKey("tappedUser") && PhoneApplicationService.Current.State["tappedUser"] != null)
                {
                    User tappedUser = (User)PhoneApplicationService.Current.State["tappedUser"];
                    string url;
                    url = Conf.SERVER_DOMAIN + session + "/conversation/participant/" + tappedUser.id_user;
                    WebRequest request = WebRequest.Create(url);
                    HttpWebRequest httpReq = (HttpWebRequest)request;
                    //request.ContentType = "application/json; charset=utf-8";
                    //request.Method = "GET";
                    try
                    {
                        httpReq.BeginGetResponse(getOneConversation, httpReq);
                    }
                    catch (WebException webex)
                    {
                        Console.WriteLine(webex.ToString());
                    }
                }
            }


            //NavigationService.Navigate(new Uri("/ConversationPage.xaml", UriKind.Relative));
        }

        public void getOneConversation(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());
                    String res = streamReader.ReadToEnd();

                    Response loginResponse = JsonConvert.DeserializeObject<Response>(res);

                    if (loginResponse.state == "ok")
                    {
                        //  List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                        UIThread.Invoke(() =>
                        {
                            string conversationId = JsonConvert.DeserializeObject<string>(loginResponse.data.ToString());
                            PhoneApplicationService.Current.State["notifedConversationId"] = conversationId;
                            NavigationService.Navigate(new Uri("/ConversationPage.xaml", UriKind.Relative));
                        });
                    }
                    else
                    {
                        // TODO
                    }
                }
                catch (WebException e)
                {
                    Console.WriteLine("Neighbors list not found");
                }
            }
        }
    }
}