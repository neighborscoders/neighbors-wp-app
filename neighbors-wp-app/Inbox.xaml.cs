﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using Newtonsoft.Json;

namespace neighbors_wp_app
{
    public partial class Inbox : PhoneApplicationPage
    {
        ProgressBar bar = new ProgressBar();
        PhoneApplicationPage from;
        public Inbox()
        {
            InitializeComponent();
        }

        public Inbox(PhoneApplicationPage from)
            : this()
        {
            this.from = from;
                InitPage();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            InitPage();
        }

        public void InitPage()
        {
            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/conversations";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                //request.ContentType = "application/json; charset=utf-8";
                //request.Method = "GET";
                try
                {
                    httpReq.BeginGetResponse(getConversations, httpReq);
                }
                catch (WebException webex)
                {
                    Console.WriteLine(webex.ToString());
                }
            }
        }

        
        public void getConversations(IAsyncResult result){
          //   StreamReader streamReader = new StreamReader(response);

            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());
                    String res = streamReader.ReadToEnd();

                    Response loginResponse = JsonConvert.DeserializeObject<Response>(res);

                    if (loginResponse.state == "ok")
                    {
                      //  List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                        object data = JsonConvert.DeserializeObject<object>(loginResponse.data.ToString());
                        List<Conversation> conversations = new List<Conversation>();

                        conversations = JsonConvert.DeserializeObject<List<Conversation>>(data.ToString());
                        displayConversations(conversations);
                    }
                    else
                    {
                        // TODO
                    }
                }
                catch (WebException e)
                {
                    Console.WriteLine("Neighbors list not found");
                }
            }
        }

        public void displayConversations(List<Conversation> conversations){
             if (conversations != null)
            {
                int conversationCount = 0;
                UIThread.Invoke(() =>
                {
                    if (this.ConversationList.Children.Count >= 0)
                    {
                        this.ConversationList.Children.Clear();
                    }
                });

                foreach (Conversation currentConversation in conversations)
                {
                    conversationCount++;
                    UIThread.Invoke(() =>
                    {
                        // Remove loader bar
                        ConversationControl conversationCardToAdd = new ConversationControl(currentConversation);
                        conversationCardToAdd.Tap += ConversationCard_Tap;
                        this.ConversationList.Children.Add(conversationCardToAdd);
                    });
                }
                if (conversationCount <= 0)
                {
                    UIThread.Invoke(() =>
                    {
                        TextBlock text = new TextBlock();
                        text.Text = "There is no messages found";
                        this.ConversationList.Children.Add(text);
                    });
                }
            }
            else
            {
                UIThread.Invoke(() =>
                {
                    TextBlock text = new TextBlock();
                    text.Text = "There is no messages found";
                    this.ConversationList.Children.Add(text);
                });
            }
        }

        private void ConversationCard_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ConversationControl currentConverastionControl = (ConversationControl)sender;
            PhoneApplicationService.Current.State["currentConversation"] = currentConverastionControl.conversation;
            if (NavigationService != null)
            {
                NavigationService.Navigate(new Uri("/ConversationPage.xaml", UriKind.Relative));
            }
            else
            {
                from.NavigationService.Navigate(new Uri("/ConversationPage.xaml", UriKind.Relative));
            }
        }
        
    }
}