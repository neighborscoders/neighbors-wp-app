﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace neighbors_wp_app
{
    public partial class HomeSettings : PhoneApplicationPage
    {
        bool isHomeNew;
        Home currentHome;
        ProgressBar bar = new ProgressBar();
        public HomeSettings()
        {
            isHomeNew = false;
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            displayAddress();
            if (PhoneApplicationService.Current.State.ContainsKey("home") && PhoneApplicationService.Current.State["home"] != null)
            {
                currentHome = (Home)PhoneApplicationService.Current.State["home"];
                updateHomeInfo(currentHome);
                PhoneApplicationService.Current.State["home"] = null;
            }
            if (PhoneApplicationService.Current.State.ContainsKey("newHome") && PhoneApplicationService.Current.State["newHome"] != null)
            {
                isHomeNew = true;
                currentHome = (Home)PhoneApplicationService.Current.State["newHome"];
                updateHomeInfo(currentHome);
                PhoneApplicationService.Current.State["newHome"] = null;
            }
        }

        private void changeAddress_Click(object sender, RoutedEventArgs e)
        {
            PhoneApplicationService.Current.State["home"] = currentHome;
            PhoneApplicationService.Current.State["oldAddress"] = currentHome.address;
            NavigationService.Navigate(new Uri("/SearchAddress.xaml", UriKind.Relative));
        }


        private void saveAddress(object sender, RoutedEventArgs e)
        {
  
            currentHome.perimeter = (int)this.PerimeterSlider.Value;
            currentHome.name = this.name.Text;
            ListPickerItem homeTypeItem =(ListPickerItem)this.HomeTypes.SelectedItem;
            if ((string)homeTypeItem.Content != "Home Type")
            {
                currentHome.type_home = (string)homeTypeItem.Content;
            }
            else
            {
                currentHome.type_home = null;
            }

            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                string url;
                url = Conf.SERVER_DOMAIN + session + "/home/save";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                request.ContentType = "application/json; charset=utf-8";
                httpReq.Method = "POST";

                httpReq.BeginGetRequestStream(sendHome, httpReq);
            }
 
        }

        public void updateHomeInfo(Home home){

            var values = Enum.GetValues(typeof(HomeType));
            foreach (var homeType in values)
            {
                ListPickerItem homeTypeItem = new ListPickerItem();
                homeTypeItem.Content = homeType.ToString();
                this.HomeTypes.Items.Add(homeTypeItem);
            }

            if (!isHomeNew)
            {
                this.name.Text = home.name;
                this.changeAddress.Content = home.address.ToString();
                this.PerimeterSlider.Value = home.perimeter;
                foreach (var item in this.HomeTypes.Items)
	            {
                    if (item is ListPickerItem)
                    {
                        ListPickerItem listItem = (ListPickerItem)item;
                        if (home.type_home == (string)listItem.Content)
                        {
                            this.HomeTypes.SelectedItem = listItem;

                        }
                    }
	            }
                
            }
        }

        private void displayAddress()
        {
            if (PhoneApplicationService.Current.State.ContainsKey("address") && PhoneApplicationService.Current.State["address"] != null)
            {

                String[] address = PhoneApplicationService.Current.State["address"].ToString().Split('/');
                this.changeAddress.Content = String.Format("{0} {1} {2} {3}",
                    address[0],
                    address[1],
                    address[2],
                    address[3]);
            }
        }

        public void sendHome(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                Stream postStream = preq.EndGetRequestStream(result);

                StringBuilder postParamBuilder = new StringBuilder();

                string jsonString = JsonConvert.SerializeObject(currentHome);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {
                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.LayoutRoot.Children.Remove(bar);
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        }

        public void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                    //   List<object> data = (List<object>)loginResponse.data; 

                    User user = JsonConvert.DeserializeObject<User>(data[0].ToString());
                    List<Home> homes = JsonConvert.DeserializeObject<List<Home>>(data[1].ToString());
                    // passer à la page index avec les bonnes données
                    PhoneApplicationService.Current.State["user"] = user;
                    PhoneApplicationService.Current.State["userHomes"] = homes;
                    UIThread.Invoke(() =>
                    {
                        this.LayoutRoot.Children.Remove(bar);
                        NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.LayoutRoot.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.LayoutRoot.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }

        private void Perimeter_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.PerimeterSlider != null)
            { 
                this.PerimeterSlider.Value = (Math.Round(e.NewValue / 0.5) / 2.0);
                this.PerimeterValue.Text = PerimeterSlider.Value.ToString();
            }
        }

    }
}