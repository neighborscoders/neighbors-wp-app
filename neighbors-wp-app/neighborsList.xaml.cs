﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using Newtonsoft.Json;

namespace neighbors_wp_app
{
    public partial class NeighborsList : PhoneApplicationPage
    {

        ProgressBar bar = new ProgressBar();

        public NeighborsList()
        {
            InitializeComponent();
            lookforNeighbors();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        public void lookforNeighbors()
        {
            // Add loader bar
            bar.IsIndeterminate = true;
            this.ContentPanel.Children.Add(bar);
           // this.ContentPanel.Children.Add(bar);

            string url;
            url = Conf.SERVER_DOMAIN;
            WebRequest request;
            request = WebRequest.Create(url);
            HttpWebRequest httpReq = (HttpWebRequest)request;
            httpReq.BeginGetResponse(getNeighborsList, request);
        }

        public void getNeighborsList(IAsyncResult result)
        {
            HttpWebRequest request = result.AsyncState as HttpWebRequest;
            if (request != null)
            {
                try
                {
                    WebResponse response = request.EndGetResponse(result);
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());
                    String resut = streamReader.ReadToEnd();
                    List<Home> neighbors = new List<Home>();
                    neighbors = JsonConvert.DeserializeObject<List<Home>>(resut);
                    displayNeighbors(neighbors);
                }
                catch(WebException e)
                {
                    Console.WriteLine("Neighbors list not found");
                }
            }
        }

        public void displayNeighbors(List<Home> neighbors)
        {
            if (neighbors != null)
            {
                foreach (Home currentHome in neighbors)
                {
                    UIThread.Invoke(() => {
                        // Remove loader bar
                        this.ContentPanel.Children.Remove(bar);

                        HomeCard homeCardToAdd = new HomeCard(currentHome);

                        this.ContentPanel.Children.Add(homeCardToAdd);
                    });
                }
            }
            else
            {
                UIThread.Invoke(() =>
                {
                    this.ContentPanel.Children.Remove(bar);
                    TextBox text = new TextBox();
                    text.Name = "There is no neighbors found";
                });
            }
        }


    }
}