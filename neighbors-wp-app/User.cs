﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class User
    {
        public int id_user { get; set; }
        public string notif_uri { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public DateTime Subscription { get; set; }
        public string photo_path { get; set; }
        public string Image { get; set; }


        public User()
        {

        }
        public User(string firstname, string lastname)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
        }
    }
}
