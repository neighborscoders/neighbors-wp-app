﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace neighbors_wp_app
{
    public partial class Subscribe : PhoneApplicationPage
    {

        ProgressBar bar = new ProgressBar();
        public Subscribe()
        {
            InitializeComponent();
        }

        private void bAddPhoto_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddPhoto.xaml", UriKind.Relative));
        }

        private void tbFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox firstNameBlock = (TextBox)sender;
                firstNameBlock.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 43, 184, 96));
                this.MessageBox.Text = "";
            }
        }

        private void tbLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox lastNameBlock = (TextBox)sender;
                lastNameBlock.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 43, 184, 96));
                this.MessageBox.Text = "";
            }

        }

        private void bRegister_Click(object sender, RoutedEventArgs e)
        {
            if (this.tbFirstName.Text.Length > 0 && this.tbLastName.Text.Length > 0)
            {
                User newUser = new User(this.tbFirstName.Text, this.tbLastName.Text);
                PhoneApplicationService.Current.State["newUser"] = newUser;
                NavigationService.Navigate(new Uri("/Subscribe2.xaml", UriKind.Relative));

            }
            else
            {
                if (this.tbFirstName.Text.Length <= 0)
                {
                    this.tbFirstName.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }

                if (this.tbLastName.Text.Length <= 0)
                {
                    this.tbLastName.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
                this.MessageBox.Text = "These fields are required";
            }
        }

        private void ContentPanel_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}