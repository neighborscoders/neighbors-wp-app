﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Windows.Storage.Streams;

namespace neighbors_wp_app
{
    public class HomeCard : HomeCardControl
    {
        string PicturePath;
        public Home Home;
        public HomeCard()
        {
            addComponents();
        }
        public HomeCard(Home home) : this()
        {
            this.HomeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-building.jpg", UriKind.Relative));
            this.HomeName.Text = home.name;
            this.HomeAddress.Text = home.address.ToString();
            this.Home = home;
            if (this.Home.distance != null)
            {
                this.Distance.Text = this.Home.distance + " m";
            }
            setHomeTypeImage(home);
        }

        private void addComponents()
        {
            Grid.SetColumn(this.HomeName, 1);
            Grid.SetRow(this.HomeName, 0);

            Grid.SetColumn(this.HomeAddress, 1);
            Grid.SetRow(this.HomeAddress, 0);
          
        }

        public void setHomeTypeImage(Home home)
        {
            switch (home.type_home)
            {
                case "Flat":
                    this.HomeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-building.jpg", UriKind.Relative));
                    break;
                case "House":
                    this.HomeImage.Source = new BitmapImage(new Uri("/Assets/Images/homeType-house.jpg", UriKind.Relative));
                    break;
            }
        }

    }
}
