﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neighbors_wp_app
{
    public class Address
    {
        public int number { get; set; }
        public string street { get; set; }
        public string street2 { get; set; }
        public int zipcode { get; set; }

        public string city { get; set; }

        public double lat { get; set; }
        public double lon { get; set; }


        public override string ToString()
        {
            string address = number + " " + street + " " + street2 + " " + zipcode + " " + city;
            return address;
        }

        public string ToSlashString()
        {
            string address = number + "/" + street + "/" + zipcode + "/" + city;
            return address;
        }
    }
}
