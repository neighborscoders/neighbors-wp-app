﻿#pragma checksum "C:\Users\Romain\documents\visual studio 2013\Projects\neighbors-wp-app\neighbors-wp-app\MessageControlRight.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "80ED9A991AD24FA978EA673CD1B1D3DF"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.34014
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace neighbors_wp_app {
    
    
    public partial class MessageControlRight : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Image MessagePicture;
        
        internal System.Windows.Controls.TextBlock MessageBlock;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/neighbors-wp-app;component/MessageControlRight.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.MessagePicture = ((System.Windows.Controls.Image)(this.FindName("MessagePicture")));
            this.MessageBlock = ((System.Windows.Controls.TextBlock)(this.FindName("MessageBlock")));
        }
    }
}

