﻿#pragma checksum "C:\Users\Romain\documents\visual studio 2013\Projects\neighbors-wp-app\neighbors-wp-app\UserCardControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4A70035F9D68222FC44C5108B803E717"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.34014
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace neighbors_wp_app {
    
    
    public partial class UserCardControl : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock UserName;
        
        internal System.Windows.Controls.TextBlock UserPseudo;
        
        internal System.Windows.Controls.Image UserImage;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/neighbors-wp-app;component/UserCardControl.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.UserName = ((System.Windows.Controls.TextBlock)(this.FindName("UserName")));
            this.UserPseudo = ((System.Windows.Controls.TextBlock)(this.FindName("UserPseudo")));
            this.UserImage = ((System.Windows.Controls.Image)(this.FindName("UserImage")));
        }
    }
}

