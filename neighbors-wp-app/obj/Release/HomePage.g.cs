﻿#pragma checksum "C:\Users\Romain\documents\visual studio 2013\Projects\neighbors-wp-app\neighbors-wp-app\HomePage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "89ED1687942574E94548FB9671B60B90"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.34014
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace neighbors_wp_app {
    
    
    public partial class HomePage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.Image HomeTypeImage;
        
        internal System.Windows.Controls.TextBlock HomeName;
        
        internal System.Windows.Controls.ScrollViewer ___Aucun_nom_;
        
        internal System.Windows.Controls.StackPanel NeiborsList;
        
        internal System.Windows.Controls.TextBlock MessageBox;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/neighbors-wp-app;component/HomePage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.HomeTypeImage = ((System.Windows.Controls.Image)(this.FindName("HomeTypeImage")));
            this.HomeName = ((System.Windows.Controls.TextBlock)(this.FindName("HomeName")));
            this.___Aucun_nom_ = ((System.Windows.Controls.ScrollViewer)(this.FindName("___Aucun_nom_")));
            this.NeiborsList = ((System.Windows.Controls.StackPanel)(this.FindName("NeiborsList")));
            this.MessageBox = ((System.Windows.Controls.TextBlock)(this.FindName("MessageBox")));
        }
    }
}

