﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace neighbors_wp_app
{
    public partial class MessageControlLeft : UserControl
    {
        Message message;
        public MessageControlLeft()
        {
            InitializeComponent();
        }
        public MessageControlLeft(Message message) : this()
        {
            this.UserFirstname.Text = message.User.Firstname+":";
            this.message = message;
            this.MessageBlock.Text = message.message;
            this.MessagePicture.Source = new BitmapImage(new Uri(message.User.photo_path));
        }
    }
}
