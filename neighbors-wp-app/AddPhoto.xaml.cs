﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace neighbors_wp_app
{
    public partial class AddPhotos : PhoneApplicationPage
    {
        PhotoChooserTask photoChooserTask;
        CameraCaptureTask photoCameraCapture; 

        public AddPhotos()
        {
            InitializeComponent();
            this.photoChooserTask = new PhotoChooserTask();
            this.photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
            this.photoCameraCapture = new CameraCaptureTask();
            this.photoCameraCapture.Completed += new EventHandler<PhotoResult>(photoCameraCapture_Completed); 
        }

        private void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            BitmapImage image = new BitmapImage();
            if (e != null && image != null && this.imgPhoto.Source != null)
            {
                image.SetSource(e.ChosenPhoto);
                this.imgPhoto.Source = image;
                PhoneApplicationService.Current.State["photo"] = image;
            }
        }

        private void photoCameraCapture_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                BitmapImage image = new BitmapImage();
                image.SetSource(e.ChosenPhoto);
                this.imgPhoto.Source = image;
                PhoneApplicationService.Current.State["photo"] = image;
            }
        } 

        private void bChoosePicture_Click(object sender, RoutedEventArgs e)
        {
            photoChooserTask.Show(); 
        }

        private void bValidPhoto_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void bTakePhoto_Click(object sender, RoutedEventArgs e)
        {
            photoCameraCapture.Show(); 
        } 

    }
}