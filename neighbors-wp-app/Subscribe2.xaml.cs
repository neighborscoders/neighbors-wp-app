﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace neighbors_wp_app
{
    public partial class Subscribe2 : PhoneApplicationPage
    {
        ProgressBar bar = new ProgressBar();
        User userToAdd;
        public Subscribe2()
        {
            InitializeComponent();
            PhoneApplicationService.Current.State["photo"] = null;
        }

        private void bAddPhoto_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddPhoto.xaml", UriKind.Relative));
        }

        private void imgProfil_Loaded(object sender, RoutedEventArgs e)
        {
            if (PhoneApplicationService.Current.State["photo"] != null)
            {
                this.imgProfil.Source = (BitmapImage) PhoneApplicationService.Current.State["photo"];
            }
        }

        private void bSignUp_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (PhoneApplicationService.Current.State["newUser"] != null)
            {
                //NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
                if (this.tbMail.Text.Length >= 0 && this.tbUsername.Text.Length >= 0 && this.tbBirthdate.Value != null && this.pbPassword.Password.Length >= 0)
                {
                    User newUser = (User)PhoneApplicationService.Current.State["newUser"];
                    newUser.Email = this.tbMail.Text;
                    newUser.Username = this.tbUsername.Text;
                    newUser.BirthDate = this.tbBirthdate.Value.Value;
                    newUser.Password = this.pbPassword.Password;
                    newUser.notif_uri = PhoneApplicationService.Current.State["uri"].ToString();

                    UIThread.Invoke(() =>
                    {
                        BitmapImage bitmapImage = (BitmapImage)imgProfil.Source;
                        if (bitmapImage != null)
                        {
                            newUser.Image = this.ImageBase64(bitmapImage);
                        }
                    });
               //     newUser.Image = this.imgProfil

                    userToAdd = newUser;

                    // Add loader bar
                    bar.IsIndeterminate = true;
                    this.ContentPanel.Children.Add(bar);                    

                    string url;
                    url = Conf.SERVER_DOMAIN + "/signup";
                    WebRequest request = WebRequest.Create(url);
                    HttpWebRequest httpReq = (HttpWebRequest)request;
                    request.ContentType = "application/json; charset=utf-8";
                    httpReq.Method = "POST";

                    httpReq.BeginGetRequestStream(registerUser, httpReq);
                }
                else
                {
                    if (this.tbMail.Text.Length <= 0)
                    {
                        this.tbMail.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    }
                    if (this.tbUsername.Text.Length <= 0)
                    {
                        this.tbUsername.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    }
                    if (this.pbPassword.Password.Length <= 0)
                    {
                        this.pbPassword.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    }
                }
            }
        }

        public void registerUser(IAsyncResult result)
        {            
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
                Stream postStream = preq.EndGetRequestStream(result);

                StringBuilder postParamBuilder = new StringBuilder();

                string jsonString = JsonConvert.SerializeObject(userToAdd);                
                Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {
                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.ContentPanel.Children.Remove(bar);
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        }


        public void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    List<object> data = JsonConvert.DeserializeObject<List<object>>(loginResponse.data.ToString());
                    //   List<object> data = (List<object>)loginResponse.data; 

                    User user = JsonConvert.DeserializeObject<User>(data[0].ToString());
                    List<Home> homes = JsonConvert.DeserializeObject<List<Home>>(data[1].ToString());
                    string session;
                    IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
                    if (String.IsNullOrEmpty(session))
                    {
                        session = JsonConvert.DeserializeObject<string>(data[2].ToString());
                        IsolatedStorageSettings.ApplicationSettings.Add("session", session);
                        IsolatedStorageSettings.ApplicationSettings.Save();
                    }
                    PhoneApplicationService.Current.State["user"] = user;                    
                    PhoneApplicationService.Current.State["userHomes"] = homes;
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        NavigationService.Navigate(new Uri("/Index.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch (JsonException e)
            {
                UIThread.Invoke(() =>
                {
                    this.ContentPanel.Children.Remove(bar);
                    this.MessageBox.Text = "Server response error";
                });
            }
        }

        public BitmapImage base64Image(string base64string)
        {
            byte[] fileBytes = Convert.FromBase64String(base64string);

            using (MemoryStream ms = new MemoryStream(fileBytes, 0, fileBytes.Length))
            {
                ms.Write(fileBytes, 0, fileBytes.Length);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(ms);
                return bitmapImage;
            }
        }

        public string ImageBase64(BitmapImage bitmapImage)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                WriteableBitmap btmMap = new WriteableBitmap(bitmapImage);

                // write an image into the stream
                Extensions.SaveJpeg(btmMap, ms, btmMap.PixelWidth, btmMap.PixelHeight, 0, 100);

                ImageSourceConverter imageConverter = new ImageSourceConverter();

                string imageString = Convert.ToBase64String(ms.ToArray());
                return imageString;
            }
        }

    }
}