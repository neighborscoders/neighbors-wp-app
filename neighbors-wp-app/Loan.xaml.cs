﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace neighbors_wp_app
{
    public partial class Loan : PhoneApplicationPage
    {
        User askedUser;
        Home askerHome;
        Home askedHome;
        string what;
        ProgressBar bar = new ProgressBar();
        public Loan()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        { 
            if (PhoneApplicationService.Current.State.ContainsKey("selectedNeighborHome") && PhoneApplicationService.Current.State["selectedNeighborHome"] != null
            && PhoneApplicationService.Current.State.ContainsKey("home") && PhoneApplicationService.Current.State["home"] != null
            && PhoneApplicationService.Current.State.ContainsKey("tappedUser") && PhoneApplicationService.Current.State["tappedUser"] != null)
            {
                askedUser = (User)PhoneApplicationService.Current.State["tappedUser"];
                askerHome = (Home)PhoneApplicationService.Current.State["home"];
                askedHome = (Home)PhoneApplicationService.Current.State["selectedNeighborHome"];
            }
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.what = this.tbWhat.Text;


            string session;
            IsolatedStorageSettings.ApplicationSettings.TryGetValue<string>("session", out session);
            if (!String.IsNullOrEmpty(session))
            {
                this.ContentPanel.Children.Add(bar);
                string url;
                url = Conf.SERVER_DOMAIN + session + "/service/loan";
                WebRequest request = WebRequest.Create(url);
                HttpWebRequest httpReq = (HttpWebRequest)request;
                request.ContentType = "application/json; charset=utf-8";
                httpReq.Method = "POST";

                httpReq.BeginGetRequestStream(saveLoan, httpReq);
            }
        }
        
        public void saveLoan(IAsyncResult result)
        {
            HttpWebRequest preq = result.AsyncState as HttpWebRequest;
            if (preq != null)
            {
            Stream postStream = preq.EndGetRequestStream(result);

            string jsonString = "{\"askedUserId\":\""+this.askedUser.id_user+"\", \"askerHomeId\":\""+this.askerHome.id_home+"\", \"askedHomeId\":\""+this.askedHome.id_home+"\", \"what\":\""+this.what+"\"}";
            Byte[] byteArray = Encoding.UTF8.GetBytes(jsonString);
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                preq.BeginGetResponse((IAsyncResult final_result) =>
                {

                    HttpWebRequest req = final_result.AsyncState as HttpWebRequest;
                    if (req != null)
                    {
                        try
                        {
                            WebResponse response = req.EndGetResponse(final_result);
                            onResponse(response.GetResponseStream());
                        }
                        catch (WebException e)
                        {
                            UIThread.Invoke(() =>
                            {
                                this.ContentPanel.Children.Remove(bar);
                                this.MessageBox.Text = "Network error, please try again later";
                            });
                        }
                    }
                }
                , preq);
            }
        
        }

        public void onResponse(Stream response)
        {
            StreamReader streamReader = new StreamReader(response);
            String result = streamReader.ReadToEnd();
            try
            {
                Response loginResponse = JsonConvert.DeserializeObject<Response>(result);

                if (loginResponse.state == "ok")
                {
                    UIThread.Invoke(() =>
                    {
                        this.MessageBox.Text = loginResponse.message;
                        this.ContentPanel.Children.Remove(bar);
                        NavigationService.Navigate(new Uri("/Inbox.xaml", UriKind.Relative));
                    });
                }
                else
                {
                    // Afficher le message d'erreur
                    UIThread.Invoke(() =>
                    {
                        this.ContentPanel.Children.Remove(bar);
                        this.MessageBox.Text = loginResponse.message;
                    });
                }
            }
            catch(WebException e)
            {

            }


        }

    }

}